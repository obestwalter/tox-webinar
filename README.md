*These are the notes I used for the webinar and the end result of that super complex, rocket sciency example project used to demonstrate the concepts.*

*I added the [.gitlab-ci.yml](.gitlab-ci.yml) afterwards to show how to get started running your tests automatically on a CI system like e.g. [gitlab pipelines](https://docs.gitlab.com/ee/ci/README.html) - here you can see how results of a [failed](https://gitlab.com/obestwalter/tox-webinar/-/jobs/236125222) and of a [successful](https://gitlab.com/obestwalter/tox-webinar/-/jobs/236133624) testrun look like.*

**[blogpost about the webinar](https://blog.jetbrains.com/pycharm/2018/11/webinar-automating-build-test-and-release-workflows-with-tox-with-oliver-bestwalter/)**

**[video of the webinar](https://youtu.be/PrAyvH-tm8E)**

# An experiment in project automation with tox

**from scratch**

## What are we going to do?

Create a complete (but simplified/idealized) develop, build, test, deploy workflow based on tox (starting from a pretty much freshly installed system) in ~45 minutes

Create a complete vertical slice through the whole process in one go leaving out many important details but giving a bird's eye perspective on how things work and how they play together.

## Some remarks

* there are promising "new" developments in building and packaging (look at flit / PEP 517/518)
* **BUT**: we'll concentrate on the currently established setup.py/setuptools workflow
* We'll always use the simplest possible approach - life is messy and there are many corner cases, but this approach works for me in most scenarios on most Osses
* I will be explicitly mentioning a lot of things that are painfully obvious for people who are experienced in Python but which are often not obvious for newcomers in packaging and automation.
* I will be leaving out a lot of things or only mention them in passing to keep it simple.
* This is live coding on a system I don't use in daily live with a keyboard layout that is alien to me - I hopefully make a complete fool of myself and make a lot of errors, which I can use to explain how things work (or realize that I don't know how things work myself :))

**This is done on macOS but portable** 

**except:**

... symlinking works differently on Windows (you might even need to create little batch helpers or look into the "new" `py` launcher - Steve Dower gave a few helpful talks about Python on Windows).

... if you work on a file system that doesn't support symlinking you need to do some extra work / settings

**Details about topics like this exceed the scope of this webinar (and often my expertise, because those are problems I don't have to deal with myself).**

---

## What is tox?

A lot of things, but in the context of this webinar: **A generic project automation and orchestration tool**

* [tox docs](https://tox.readthedocs.io)

## Get started from scratch 

**Preparation I did on a fresh mac (supposed to be a developer box - good CI systems need no manual preparation)**

## No Python3? Not acceptable - get some proper python

* [download 3.6](https://www.python.org/ftp/python/3.6.7/python-3.6.7-macosx10.6.pkg)
* run the installer and wade through the menu, choose all the defaults (make sure that python is added to the path)

Looks like it works.

```console
schmubby:~ oliver.bestwalter$ python3 
Python 3.6.7 (v3.6.7:6ec5cf24b7, Oct 20 2018, 00:28:22)
```

## Install tox in a dedicated venv

    $ cd
    $ python3 -m venv .toxbase
    # no need to activate ...
    $ .toxbase/bin/pip list  # not much, but we have pip now
    $ .toxbase/bin/pip install tox
    $ .toxbase/bin/tox --version  # see? no activation :)
    
## Make tox available as a generic command line tool

    $ mkdir ~/bin
    
**add a user bin dir to the path for your tools (in .bash_profile or your equivalent)**

    PATH="/Library/Frameworks/Python.framework/Versions/3.6/bin:${PATH}"
    PATH="${HOME}/bin:${PATH}"
    export PATH

## Link tox executable from venv into local bin

```console
$  ln -s /Users/oliver.bestwalter/.toxbase/bin/tox /Users/oliver.bestwalter/bin/tox
```

```console
$ which tox
/Users/oliver.bestwalter/.toxbase/bin/tox
$ tox --version
3.5.3 imported from /Users/oliver.bestwalter/.toxbase/lib/python3.6/site-packages/tox/__init__.py
```

# Now we can get started

## Naive approach

* cd projects
* mkdir jbw
* touch tox.ini
* tox

## Less naive approach

* rm tox.ini
* tox-quickstart

## Explicit approach

Defining a testenv can be done explicit

```ini
[tox]
skipsdist = True

[testenv]
basepython = python3

[testenv:dev3]
description = {envpython}
commands =
    python --version
    python -c "import sys; print(sys.executable)"

[testenv:dev2]
description = {envpython}
basepython = python2.7
commands =
    python --version
    python -c "import sys; print(sys.executable)"
```

### what options are there to debug problems?

* `tox --showconfig`
* if all seems o.k.: before looking further - always do this first if it worked, but doesn't anymore: `-r | --recreate`
* `tox -vvvvvv` - 3 for tox and three for pip
* run commands as part of your toxrun to give you more information
* run tox programmatically and attach a debugger to the process (see [tox developer faq](https://tox.readthedocs.io/en/latest/developers.html) and [cmdline](https://github.com/tox-dev/tox/blob/7c9d5d247a777193bfb89eca997227a74bca1be3/src/tox/session.py#L39))

## Now let's build up a little project

* simple script
* test without pytest and test?
* simple test
* add pytest
* create a package
* move tests
* create an installable project
* Pythons best kept dev secret `pip install --editable`
* add linting with black and flake8
* add pre-commit
* add coverage
* docs
* release
* etc (all the same from tox' perspective)
* What does that look like on CI then?
